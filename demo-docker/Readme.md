## Summary
This is a project to demonstrate the fabric8 docker-maven-plugin. It will start up docker containers to use for integration testing.

The docker containers are tied to the integration-test lifecycle pre and post phases. As such any time the pre-integration-test phase is run, docker containers will get built and run. When the post-integration-test phase is run, the running docker containers are removed.

This is all configured in the plugin in the build section of the pom.

#### Running Tests
To run just the unit tests, run `mvn clean package`

To run both the unit tests and the integration tests run `mvn clean verify`

#### Running Containers
It is also possible to start and stop the containers. The main methods to use are:

`mvn docker:build` - This will build the containers but not run them.  
`mvn docker:start` - This will run the containers. If they have not already been built an error will be thrown.  
`mvn docker:stop` - This will stop the running containers are remove them, but not remove the images  
`mvn -Ddocker.removeAll docker:remove` This will remove all of the images  
`mvn -Ddocker.filter=todo-server -Ddocker.removeAll docker:remove` - This will remove the todo-server image but leave any other images (currently mongo)  

#### More Information
If you would like more information regarding the plugins please see [https://dmp.fabric8.io](https://dmp.fabric8.io)

### Dockerfile Configuration
This also has an example Dockerfile and docker-compose.yml for example. Unfortunately, currently, this plugin only supports version 2 of the docker-compose format.