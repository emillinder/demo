package com.bcknds.demo.docker.service;

import com.bcknds.demo.docker.model.Todo;
import com.bcknds.demo.docker.repository.TodoRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoServiceImpl implements TodoService {

    private TodoRepository todoRepository;

    @Autowired
    public TodoServiceImpl(final TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public List<Todo> findAll(final Pageable pageable) {
        return Lists.newArrayList(todoRepository.findAll(pageable));
    }

    @Override
    public Todo findOne(final String id) {
        return todoRepository.findOne(id);
    }

    @Override
    public Todo save(final Todo todo) {
        return todoRepository.save(todo);
    }

    @Override
    public void remove(final String id) {
        todoRepository.delete(id);
    }
}
