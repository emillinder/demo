package com.bcknds.demo.docker.service;

import com.bcknds.demo.docker.model.Todo;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TodoService {

    List<Todo> findAll(Pageable pageable);
    Todo findOne(String id);
    Todo save(Todo todo);
    void remove(String id);

}
