package com.bcknds.demo.docker.controller;

import com.bcknds.demo.docker.model.Todo;
import com.bcknds.demo.docker.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(
        value = "/",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class TodoController {

    private TodoService todoService;

    @Autowired
    public TodoController(final TodoService todoService) {
        this.todoService = todoService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Todo> findAll(@PageableDefault final Pageable pageable) {
        return todoService.findAll(pageable);
    }

    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.GET
    )
    public Todo findOne(@PathVariable("id") final String id) {
        return todoService.findOne(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public Todo create(@RequestBody final Todo todo) {
        return todoService.save(todo);
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public Todo update(@RequestBody final Todo todo) {
        return todoService.save(todo);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE
    )
    public void remove(@PathVariable("id") final String id) {
        todoService.remove(id);
    }
}
