package com.bcknds.demo.docker.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "todo")
public class Todo {

    @Id
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Todo)) return false;

        Todo todo = (Todo) o;

        if (getId() != null ? !getId().equals(todo.getId()) : todo.getId() != null) return false;
        return getName() != null ? getName().equals(todo.getName()) : todo.getName() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public static class Builder {
        private Todo todo;

        public Builder() {
            this.todo = new Todo();
        }

        public Builder(final String name) {
            this();
            this.todo.name = name;
        }

        public Builder(final String id, final String name) {
            this(name);
            this.todo.id = id;
        }

        public Builder id(final String id) {
            this.todo.id = id;
            return this;
        }

        public Builder name(final String name) {
            this.todo.name = name;
            return this;
        }

        public Todo build() {
            return this.todo;
        }
    }
}
