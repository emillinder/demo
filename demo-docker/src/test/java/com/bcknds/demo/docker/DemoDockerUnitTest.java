package com.bcknds.demo.docker;

import com.bcknds.demo.docker.model.Todo;
import com.bcknds.demo.docker.repository.TodoRepository;
import com.bcknds.demo.docker.service.TodoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DemoDockerUnitTest {

    private static final String MOCK_ID = "1234567890";


	@Mock
    private TodoRepository todoRepository;

	@InjectMocks
    private TodoServiceImpl todoService;

	@Test
	public void contextLoads() {
	    when(todoRepository.findOne(MOCK_ID)).thenReturn(new Todo.Builder(MOCK_ID, "Do the dishes").build());
	    Todo todo = todoService.findOne(MOCK_ID);
	    assertNotNull(todo);
	    assertNotNull(todo.getId());
	    assertEquals("Do the dishes", todo.getName());
	}

}
