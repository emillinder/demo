package com.bcknds.demo.docker;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;

public class DemoDockerIntegrationTest {

	private static final Logger logger = LoggerFactory.getLogger(DemoDockerIntegrationTest.class);

	@Test
	public void when_get_then_200() {

        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080", String.class);
        assertEquals(200, response.getStatusCodeValue());

	}

}
