package com.bcknds.demo.h2.model;

import java.math.BigInteger;

public class StoredProcedure {

    private StoredProcedure() {}

    public static boolean isPrime(int value) {
        return new BigInteger(String.valueOf(value)).isProbablePrime(100);
    }

}
