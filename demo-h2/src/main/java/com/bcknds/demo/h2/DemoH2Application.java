package com.bcknds.demo.h2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@SpringBootApplication
@SuppressWarnings("squid:S1118")
@Order(20)
public class DemoH2Application implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(DemoH2Application.class);

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("squid:S2095")
	public static void main(String[] args) {
		SpringApplication.run(DemoH2Application.class, args);
	}

    @Override
    public void run(String... strings) throws Exception {
        for (int i = 0; i < 30; i++) {
            logger.info("{} - {}", i, isNumberPrime(i));
        }

    }

    private boolean isNumberPrime(int value) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("IS_PRIME");

        // Named parameter way of doing it, but apparently the way H2 does procedures, named parameters are not
        //   supported therefore the commented out section

//        String paramName = "value";
//        storedProcedure.registerStoredProcedureParameter(paramName, Integer.class, ParameterMode.IN);
//        storedProcedure.setParameter(paramName, value);

        storedProcedure.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
        storedProcedure.setParameter(1, value);

        return (boolean) storedProcedure.getResultList().get(0);
    }
}
