## Summary
Project to show how to work with an embedded H2 database

## Configuration
Uses the default Spring Boot set up for embedded H2. It also applies (by convention of Spring Boot) the schema.sql file in the classpath.

## H2 Console
The H2 console runs at the endpoint /h2-console. To log in use jdbc:h2:mem:testdb for the url, as for the username and no password.

## Stored Procedures
H2 does not support stored procedures by default so there is an ALIAS set up that runs a Java function that simulates a stored procedure. This alias is created in the schema.sql file and points to the isPrime method in the StoredProcedure class.

It can be used like any stored procedure with some limitations. There is no support for named parameters and out parameters do not work. Keep these limitations in mind if using this for testing.
