package com.bcknds.demo.hikari.repository;

import com.bcknds.demo.hikari.model.Todo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "todos", path = "todo")
public interface TodoRepository extends PagingAndSortingRepository<Todo, Long> {
}
