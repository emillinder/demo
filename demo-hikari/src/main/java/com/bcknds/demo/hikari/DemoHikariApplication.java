package com.bcknds.demo.hikari;

import com.bcknds.demo.hikari.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings({"squid:S2095", "squid:S1118"})
public class DemoHikariApplication implements CommandLineRunner {

    private TodoRepository todoRepository;

    @Autowired
    public DemoHikariApplication(final TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

	public static void main(String[] args) {
		SpringApplication.run(DemoHikariApplication.class, args);
	}

    @Override
    public void run(String... strings) throws Exception {
//        todoRepository.save(new Todo.Builder().name("do the dishes").build());
    }
}
