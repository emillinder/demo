## Summary
Project to demonstrate the use of the built-in Spring Boot Hikari datasource

## Usage
There are two profiles. The default profile which uses application.properties and uses an embedded h2 instance. The other is mysql which uses application-mysql.properties to connect to a mysql database. Both of these are setup in a create-drop format where all data is lost upon restart.

## Configuration
Spring boot will automatically make Hikari available if it is on the classpath. For Maven, use the following dependency:

```
<dependency>
  <groupId>com.zaxxer</groupId>
  <artifactId>HikariCP</artifactId>
</dependency>
```