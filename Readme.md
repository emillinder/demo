## Summary
Projects to demo and document how to accomplish various coding tasks.

## demo-injection
Demonstrates various Dependency Injection methods.

## demo-properties
Project to show various states of using @ConfigurationProperties and @Value annotations.

## demo-admin
Project to demonstrate the basic use of Spring Boot Admin and the Actuator endpoints.

## demo-docker
This is a project to demonstrate the fabric8 docker-maven-plugin. It will start up docker containers to use for integration testing.

## demo-h2
Project to show how to work with an embedded H2 database

## demo-hikari
Project to demonstrate the use of the built-in Spring Boot Hikari datasource
