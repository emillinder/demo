## Summary
Project to demonstrate the basic use of Spring Boot Admin and the Actuator endpoints.

## Accessing
To get to the Spring Boot Admin interface, open a browser and go to http://localhost:8080.

For actuator - go to http://localhost:8080/actuator

## Profiles
There are two profiles in the system. There is an insecure profile that shows how everything will work without security and a secure one that shows a working system with security. To specify a profile, run the application with the --spring.profiles.active=<profile> parameters. If both insecure and secure profiles are chosen, insecure will when because the secure profile uses the default configuration and is overriden by the insecure profile.

#### Insecure Profile
For the insecure profile, everything is wide open. This is accomplished by turning of security on the actuator endpoints and using the InsecureConfiguration class for security. This can be activated by using the insecure Spring Boot profile (active by default).

#### Secure profile
This profile is set up with http basic security. It uses the default configurations with Spring Security on the classpath with the exception of specifying the user. The default user is admin/admin. This has the actuator endpoints that are secure by default still secure and the default security of basic authentication required for all endpoints.

## Notes
The Hateoas library is required to view the /actuator endpoint as it is a registry of all of the other endpoints.

This is both an admin server as well as client. For a production system, you might wish to separate the server into it's own project.