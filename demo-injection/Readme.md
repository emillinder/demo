## Summary
Demonstrates various Dependency Injection methods.
 
##### Dependency Injection Methods 
There are three main methods.

- Field Injection - Autowiring a field directly (FieldInjectedService)
- Constructor Injection - Injecting a dependency into the constructor (ConstructorInjectedService)
- Setter Injection - Injecting a dependency using a setter (SetterInjectedService)

##### Duplicate Beans
If there are multiple beans of the type that is trying to be injected, then use @Qualifier to differentiate between them. There are two TodoRepository beans being created and that is the reason for the @Qualifier in the above classes. If there is only one of that type then @Qualifier is not necessary (TodoServiceImpl).

##### Non-Required Dependencies
@Autowired does take a parameter of required so if the bean is not a required dependency this can be set to false. @Autowired(required = false)