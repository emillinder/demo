package com.bcknds.demo.injection.repository;

import com.bcknds.demo.injection.model.Todo;

import java.util.List;

public interface TodoRepository {

    List<Todo> findAll();
    Todo findOne(String id);
    Todo save(Todo todo);
    void remove(String id);

}
