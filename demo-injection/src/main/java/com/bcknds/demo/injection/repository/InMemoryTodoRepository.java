package com.bcknds.demo.injection.repository;

import com.bcknds.demo.injection.model.Todo;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Repository
public class InMemoryTodoRepository implements TodoRepository {

    private volatile Map<String, Todo> todos;

    public InMemoryTodoRepository() {
        todos = new HashMap<>();
    }

    @Override
    public synchronized List<Todo> findAll() {
        return Lists.newArrayList(todos.values());
    }

    @Override
    public synchronized Todo findOne(String id) {
        return todos.get(id);
    }

    @Override
    public synchronized Todo save(Todo todo) {
        if (null == todo.getId()) {
            todo = new Todo.Builder().id(UUID.randomUUID().toString()).name(todo.getName()).build();
        }
        todos.put(todo.getId(), todo);
        return todo;
    }

    @Override
    public synchronized void remove(String id) {
        todos.remove(id);
    }
}
