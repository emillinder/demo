package com.bcknds.demo.injection.service;

import com.bcknds.demo.injection.model.Todo;
import com.bcknds.demo.injection.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConstructorInjectedService implements TodoReadService {

    private TodoRepository todoRepository;

    @Autowired
    public ConstructorInjectedService(@Qualifier("intermediateTodoRepository") TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public List<Todo> findAll() {
        return todoRepository.findAll();
    }

    @Override
    public Todo findOne(String id) {
        return todoRepository.findOne(id);
    }
}
