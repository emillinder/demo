package com.bcknds.demo.injection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoInjectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoInjectionApplication.class, args);
	}
}
