package com.bcknds.demo.injection.service;

import com.bcknds.demo.injection.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SetterInjectedService implements TodoRemovalService {

    private TodoRepository todoRepository;

    @Override
    public void remove(String id) {
        todoRepository.remove(id);
    }

    @Autowired
    @Qualifier("inMemoryTodoRepository")
    public void setTodoRemovalService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }
}
