package com.bcknds.demo.injection.service;

import com.bcknds.demo.injection.model.Todo;

public interface TodoMutableService {

    Todo save(Todo todo);

}
