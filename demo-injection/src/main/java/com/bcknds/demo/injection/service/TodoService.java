package com.bcknds.demo.injection.service;


import com.bcknds.demo.injection.model.Todo;

import java.util.List;

public interface TodoService {

    List<Todo> findAll();
    Todo findOne(String id);
    Todo save(Todo todo);
    void remove(String id);

}
