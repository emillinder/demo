package com.bcknds.demo.injection.service;

import com.bcknds.demo.injection.model.Todo;

import java.util.List;

public interface TodoReadService {

    List<Todo> findAll();
    Todo findOne(String id);

}
