package com.bcknds.demo.injection.repository;

import com.bcknds.demo.injection.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IntermediateTodoRepository implements TodoRepository {

    private TodoRepository todoRepository;

    @Autowired
    public IntermediateTodoRepository(@Qualifier("inMemoryTodoRepository") final TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public List<Todo> findAll() {
        return todoRepository.findAll();
    }

    @Override
    public Todo findOne(String id) {
        return todoRepository.findOne(id);
    }

    @Override
    public Todo save(Todo todo) {
        return todoRepository.save(todo);
    }

    @Override
    public void remove(String id) {
        todoRepository.remove(id);
    }
}
