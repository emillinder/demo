package com.bcknds.demo.injection.service;

import com.bcknds.demo.injection.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class uses constructor injection. What is interesting is that the @Autowired is not truly necessary. It can be
 * removed and Spring will still autowire the repository
 */
@Service
public class NoQualifierMultipleService implements TodoService {

    private TodoReadService todoReadService;
    private TodoMutableService todoMutableService;
    private TodoRemovalService todoRemovalService;

    @Autowired
    public NoQualifierMultipleService(TodoReadService todoReadService, TodoMutableService todoMutableService, TodoRemovalService todoRemovalService) {
        this.todoReadService = todoReadService;
        this.todoMutableService = todoMutableService;
        this.todoRemovalService = todoRemovalService;
    }

    @Override
    public List<Todo> findAll() {
        return todoReadService.findAll();
    }

    @Override
    public Todo findOne(final String id) {
        return todoReadService.findOne(id);
    }

    @Override
    public Todo save(final Todo todo) {
        return todoMutableService.save(todo);
    }

    @Override
    public void remove(final String id) {
        todoRemovalService.remove(id);
    }
}
