package com.bcknds.demo.injection.service;

import com.bcknds.demo.injection.model.Todo;
import com.bcknds.demo.injection.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class FieldInjectedService implements TodoMutableService {

    @Autowired
    @Qualifier("intermediateTodoRepository")
    private TodoRepository todoRepository;

    @Override
    public Todo save(Todo todo) {
        return todoRepository.save(todo);
    }
}
