package com.bcknds.demo.injection.service;

public interface TodoRemovalService {

    void remove(String id);

}
