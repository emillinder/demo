## Summary
Project to show various states of using @ConfigurationProperties and @Value annotations.

In order to use @ConfigurationProperties, you must have @EnableConfigurationProperties somewhere in your application.

##### Rules for @ConfigurationProperties
The overarching rule to use @ConfigurationProperties is to use both getters and setters for the properties. This will allow them to be set as well as type-ahead will work and they will show up in the /configprops endpoint as well.

If setters only are applied to values, they will work, however type-ahead in the properties (and yaml) files will not work nor will they show up in the /configprops endpoint.

With no getters or setters, nothing will work. The properties will NOT get set.

##### Rules for @Value
@Value does not require getters or setters to work. However, at this time I've been unable to get type-ahead to work and they do not show up in the /configprops endpoint.

#####
Notes: For changes to take effect in regards to type-ahead, the project must be recompiled. This recompilation generates a file (/target/classes/META-INF/spring-configuration-metadata.json) that contains the type-ahead custom configuration.

 