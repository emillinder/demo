package com.bcknds.demo.properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class DemoPropertiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPropertiesApplication.class, args);
	}
}
