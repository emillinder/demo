package com.bcknds.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class represents properties that will be configured and used, but will not show up in the actuator configprops
 * url nor will type-ahead work in the properties files because there are no getters.
 */
@Configuration
@ConfigurationProperties(prefix = "broken")
public class BrokenConfiguration {

    String host;
    int port;

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
