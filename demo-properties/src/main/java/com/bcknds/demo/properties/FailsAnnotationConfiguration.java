package com.bcknds.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class will not work at all because there are no setters
 */
@Configuration
@ConfigurationProperties(prefix = "fails")
public class FailsAnnotationConfiguration {

    String host;
    int port;
}
