package com.bcknds.demo.properties;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoPropertiesApplicationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
    private AnnotationConfiguration annotationConfiguration;

	@Autowired
	private BrokenConfiguration brokenConfiguration;

	@Autowired
    private FailsAnnotationConfiguration failsAnnotationConfiguration;

	@Autowired
    private ValueConfiguration valueConfiguration;

    @Test
    public void test_annotation_properly_configured() {
        assertEquals("annotation.host", annotationConfiguration.getHost());
        assertEquals(8081, annotationConfiguration.getPort());
    }

    @Test
    public void test_annotation_actuator_contains_property() {
        ResponseEntity<JsonNode> response = restTemplate.getForEntity("/configprops", JsonNode.class);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("annotation.host", response.getBody().get("annotationConfiguration").get("properties").get("host").asText());
        assertEquals(8081, response.getBody().get("annotationConfiguration").get("properties").get("port").asInt());
    }

    @Test
    public void test_broken_properly_configured() {
        assertEquals("broken.host", brokenConfiguration.host);
        assertEquals(8082, brokenConfiguration.port);
    }

    @Test
    public void test_broken_actuator_contains_class_no_properties() {
        ResponseEntity<JsonNode> response = restTemplate.getForEntity("/configprops", JsonNode.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody().get("brokenConfiguration"));
        assertEquals(0, response.getBody().get("brokenConfiguration").get("properties").size());
    }

    @Test
    public void test_fails_not_properly_configured() {
        assertNull(failsAnnotationConfiguration.host);
        assertEquals(0, failsAnnotationConfiguration.port);
    }

    @Test
    public void test_fails_actuator_contains_class_no_properties() {
        ResponseEntity<JsonNode> response = restTemplate.getForEntity("/configprops", JsonNode.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody().get("failsAnnotationConfiguration"));
        assertEquals(0, response.getBody().get("failsAnnotationConfiguration").get("properties").size());
    }

    @Test
    public void test_value_properly_configured() {
        assertEquals("value.host", valueConfiguration.host);
        assertEquals(8084, valueConfiguration.port);
        assertEquals("value.host:8084", valueConfiguration.getUrl());
    }

    @Test
    public void test_value_actuator_no_class() {
        ResponseEntity<JsonNode> response = restTemplate.getForEntity("/configprops", JsonNode.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNull(response.getBody().get("valueConfiguration"));
    }
}
